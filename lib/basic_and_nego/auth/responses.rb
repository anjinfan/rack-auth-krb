module BasicAndNego
  module Auth
    module Responses

      def unauthorized(options={})
        challenge = options[:challenge]
        challenge = ['Negotiate', 'Basic'] if challenge.nil? || challenge.empty?
        body = []
        body = [<<-HTML
          <html>
            <head>
              <meta http-equiv="REFRESH" content="#{options[:newauth_redirect_timeout]};url=#{options[:newauth_redirect_path]}">
            </head>
            <body>
              <h1>WebSSO not available</h1>
              <p>Click <a href="#{options[:newauth_redirect_path]}">here</a> if not automatically redirected.</p>
            </body>
          </html>
        HTML
        ] if challenge.include? 'Newauth'
        [ 401,
          { 'Content-Type' => 'text/html',
            'WWW-Authenticate' => challenge },
          body
        ]
      end

      def bad_request
        [ 400,
          { 'Content-Type' => 'text/plain',
            'Content-Length' => '0' },
            []
        ]
      end

      def error
        [ 500,
          { 'Content-Type' => 'text/plain',
            'Content-Length' => '0' },
            []
        ]
      end

    end
  end
end
